
import {dom} from '../dom/dom';
import {events} from '../events/events';
import EventManager from '../events/EventManager';
import {ElementData} from '../dom/ElementData';

EventManager.add_action('element/inview?trigger=counter', function(element, is_in)
{
	if(is_in)
	{
		dom.addClass(element, 'inview');

		let counter				= element.querySelector('[data-counter]');
		let data				= ElementData.get('counter', counter, {from: 0, step : 5});

		let value				= counter.innerHTML;
		let commas				= value.indexOf(',') > -1;

		let target_value		= Number(value.replace (/[^0-9]/g, '')),
			current_value		= data.from;

		let interval			= setInterval(function()
		{
			current_value	   += data.step;

			if(current_value > target_value)
			{
				current_value	= target_value

				clearInterval(interval);
			}

			counter.innerHTML	= commas ? current_value.toLocaleString() : current_value;
		}, 10);
	}

}, 10, 2);
